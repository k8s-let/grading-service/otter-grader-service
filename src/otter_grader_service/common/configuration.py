import os

# General
loglevel = os.getenv("LOGLEVEL", "INFO")
deployment_type = os.getenv("DEPLOYMENT_TYPE", "dev")  # queue and user suffices
cert_directory = os.getenv("CERT_DIRECTORY", "/etc/rabbitmq/certs")

# Queue connection parameters
rabbitmq_host = os.getenv("RABBITMQ_HOST", "localhost")
rabbitmq_virtualhost = os.getenv(
    "RABBITMQ_VIRTUALHOST", f"ottergrader-{deployment_type}"
)
rabbitmq_user = os.getenv("RABBITMQ_USER", f"otter-{deployment_type}")
rabbitmq_password = os.getenv("RABBITMQ_PASSWORD")
stay_connected = os.getenv(
    "STAY_CONNECTED", "on"
)  # retry connections if queue not available
rabbitmq_cert_file = os.getenv(
    "RABBITMQ_CERT_FILE", "cert_otter-client-dev.ethz.ch.pem"
)
rabbitmq_key_file = os.getenv("RABBITMQ_KEY_FILE", "otter-client-dev.ethz.ch.key")
rabbitmq_ca_file = os.getenv("RABBITMQ_CA_FILE", "ETHZ_CA_chain.pem")

# Server
in_token = os.getenv("IN_TOKEN")

# Grader
docker_cmd = os.getenv("DOCKER_CMD", "docker")
grading_home = os.getenv("GRADING_HOME", "/home/jovyan")
grading_image = os.getenv(
    "GRADING_IMAGE", "registry.ethz.ch/k8s-let/notebooks/jh-notebook-universal:3.0.0-36"
)

# Responder
moodle_host = os.getenv("MOODLE_HOST", "http://localhost:8001")
out_token = os.getenv("OUT_TOKEN")
drop_undeliverable = os.getenv("DROP_UNDELIVERABLE", False)
