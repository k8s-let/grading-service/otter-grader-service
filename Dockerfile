FROM python:3.11-alpine
LABEL authors="Bengt Giger <bgiger@ethz.ch>"
LABEL maintainer="Bengt Giger <bgiger@ethz.ch>"
ENV PYTHONUNBUFFERED=1

# Use artifacts created in previous step
COPY dist/* .
# Version tagged image used to have the same hash like the previous build.
# The installed Python package was then the pre-tag version.
# This ensures unique builds, and the tagged package version.
#RUN date >/.timestamp
RUN pip install *.whl

RUN addgroup py && adduser -D -G py py
USER py:py

EXPOSE 8000
CMD gunicorn -b 0.0.0.0:8000 -t 120 -w 4 --worker-class uvicorn.workers.UvicornWorker otter_grader_service.pusher.main:app
