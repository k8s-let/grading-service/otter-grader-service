# Development

## Environment
Check out the repository. Preferably, use an IDE like PyCharm for the development environment. Create
a virtual environment for the dependencies and activate it. Either use the IDE (*Add Python interpreter*
in PyCharm), or use `python -m venv ...`.

## Dependencies
Install the `otter-grader-service` package as an editable live package: from the root directory
of the local repository clone, issue

```commandline
pip install -e .
```

## Pre-commit
Git commits will be reviewed by sanitizing and security checks like [flake8](https://github.com/PyCQA/flake8)
or [bandit](https://github.com/PyCQA/bandit). The build pipeline will abort if any test fails.

The testing framework has been installed in the venv together with the build tools. To run the tests locally, initialize
them with:

```commandline
pre-commit install
```

PyCharm will now automatically perform a pre commit check on every commit attempt. To run it manually, use:

```commandline
pre-commit run -a
```

Some errors will be fixed automatically, like code reformat or CR/LF issues.
Fix all remaining errors before committing!

## Versioning
The version is set automatically by *hatch-vcs*. The base version is taken from the git tag. If
there are any commits after the last tag, a suffix is added.

Note: don't add the file `_version.py` to git!
